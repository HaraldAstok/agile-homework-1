defmodule Yahtzee do
  def score_upper(dice) do
    %{Ones: length(Enum.filter(dice, fn e -> e == 1 end)),
    Twos: length(Enum.filter(dice, fn e -> e == 2 end)),
    Threes: length(Enum.filter(dice, fn e -> e == 3 end)),
    Fours: length(Enum.filter(dice, fn e -> e == 4 end)),
    Fives: length(Enum.filter(dice, fn e -> e == 5 end)),
    Sixes: length(Enum.filter(dice, fn e -> e == 6 end))
  }
  end

  def score_lower(dice) do
    %{"Three of a kind": three_of_a_kind(dice, list_elements(dice)),
    "Four of a kind":  four_of_a_kind(dice, list_elements(dice)),
    "Full house": full_house(list_elements(dice)),
    "Small straight": small_straight(dice),
    "Large straight": large_straight(dice),
    "Yahtzee": yahtzee(list_elements(dice)),
    "Chance": Enum.sum(dice)
  }
  end 

  #make a list of how many what element list contains
  # eg. [1,2,2,3,5] -> [1,2,1,0,1,0]
  def list_elements(dice) do
    [Enum.count(dice, fn e -> e == 1 end), 
    Enum.count(dice, fn e -> e == 2 end), 
    Enum.count(dice, fn e -> e == 3 end), 
    Enum.count(dice, fn e -> e == 4 end),
    Enum.count(dice, fn e -> e == 5 end), 
    Enum.count(dice, fn e -> e == 6 end),
  ]
  end  

  def three_of_a_kind(dice, list) do
    if Enum.member?(list, 3) or Enum.member?(list, 4) or Enum.member?(list, 5) do
      Enum.sum(dice)
    else
      0
    end
  end

  def four_of_a_kind(dice, list) do
    if Enum.member?(list, 4) or Enum.member?(list, 5)  do
      Enum.sum(dice)
    else
      0
    end
  end

  def full_house(list) do 
    if Enum.member?(list, 3) and Enum.member?(list, 2) or Enum.member?(list, 5) do
      25
    else
      0
    end
  end

  def one_to_four(dice) do
    if Enum.member?(dice, 1) and Enum.member?(dice, 2) and Enum.member?(dice, 3) and Enum.member?(dice, 4) do
      true
    else
      false
    end
  end

  def two_to_five(dice) do
    if Enum.member?(dice, 2) and Enum.member?(dice, 3) and Enum.member?(dice, 4) and Enum.member?(dice, 5) do
      true
    else
      false
    end
  end

  def three_to_six(dice) do
    if Enum.member?(dice, 3) and Enum.member?(dice, 4) and Enum.member?(dice, 5) and Enum.member?(dice, 6) do
      true
    else
      false
    end
  end

  def small_straight(dice) do 
    if one_to_four(dice) or two_to_five(dice) or three_to_six(dice) do
      30
    else
      0
    end
  end

  def large_straight(dice) do
    if Enum.sort(dice) == [1,2,3,4,5] or Enum.sort(dice) == [2,3,4,5,6] do
      40
    else
      0
    end
  end

  def yahtzee(list) do
    if Enum.member?(list, 5) do
      50
    else
      0
    end
  end

end